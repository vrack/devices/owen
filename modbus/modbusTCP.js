
module.exports = class ModbusTCP {
  socket = null;
  index = 1;
  protocol = 0;
  timeout = 2000;
  queue = new Map();
  queueTimers = new Map();
  queueErrorCB = new Map();

  constructor (socket) {
    this.socket = socket
    socket.on('data', (data) => {
      var func
      try {
        var pkg = this.splitPkg(data)
        if (this.queue.has(pkg.index)) {
          clearTimeout(this.queueTimers.get(pkg.index))
          func = this.queue.get(pkg.index)
          this.queue.delete(pkg.index)
          this.queueTimers.delete(pkg.index)
          this.queueErrorCB.delete(pkg.index)
          func(pkg)
        }
      } catch (error) {
        if (data.length >= 2) {
          var index = data.readUInt16BE(0)
          if (this.queueErrorCB.has(index)) {
            func = this.queueErrorCB.get(index)
            func(error)
            this.queue.delete(index)
            this.queueTimers.delete(index)
            this.queueErrorCB.delete(index)
          }
        }
      }
    })
  }

  commandPromise (address, command, start, count, data) {
    return new Promise((resolve, reject) => {
      this.command(address, command, start, count, data, resolve, reject)
    })
  }

  command (address, command, start, count, data, successCB, errorCB) {
    this.index++
    if (this.index > 65534) this.index = 1
    const li = this.index
    const pkg = this.createPkg(li, address, command, start, count, data)
    this.queue.set(li, successCB)
    this.queueErrorCB.set(li, errorCB)
    this.queueTimers.set(li, setTimeout(() => {
      this.queueTimers.delete(li)
      this.queue.delete(li)
      this.queueErrorCB.delete(li)
      errorCB(Error('Timeout'))
    }, this.timeout))
    this.socket.write(pkg)
  }

  createPkg (index, address, command, start, count, data) {
    var buffer = Buffer.alloc(12)
    buffer.writeUInt16BE(index, 0)
    buffer.writeUInt16BE(this.protocol, 2)
    buffer.writeUInt16BE(6, 4)
    buffer.writeUInt8(address, 6)
    buffer.writeUInt8(command, 7)
    buffer.writeUInt16BE(start, 8)
    buffer.writeUInt16BE(count, 10)
    return buffer
  }

  splitPkg (buffer) {
    if (buffer.length < 9) throw new Error('Error buffer length < 9')
    const result = {}
    result.index = buffer.readUInt16BE(0)
    result.protocol = buffer.readUInt16BE(2)
    result.size = buffer.readUInt16BE(4)
    result.address = buffer.readUInt8(6)
    result.command = buffer.readUInt8(7)
    result.dataSize = buffer.readUInt8(8)
    result.data = buffer.subarray(9, 9 + result.dataSize)
    return result
  }
}
