const MB210Basic = require('./MB210Basic')
const { Rule, Port, Action } = require('vrack-core')
module.exports = class extends MB210Basic {
  ports () {
    return [
      new Port('gate').input().data('signal')
        .description('Вход для провайдера'),
      new Port('cn%d').output().data('number').dynamic(20)
        .description('Значение дискретного входа')
    ]
  }

  checkParams () {
    return [
      new Rule('eachGate').required().default(true).isBoolean()
    ]
  }

  settings () {
    return {
      messageTypes: ['error', 'event']
    }
  }

  actions () {
    return [
      new Action('update.channel').rules([
        new Rule('channel').required().default(1).isInteger().expression('value >= 1 && value <= 20'),
        new Rule('offset').required().default(0).isNumber(),
        new Rule('weight').required().default(1).isNumber()
      ]).return('null').description('Устанавливает вес импульса и оффсет для канала')

    ]
  }

  shares = {
    online: false,
    progress: false,
    cn: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  }

  preProcess () {
    if (this.storage.channels === undefined) this.storage.channels = {}
    for (let i = 1; i <= 20; i++) {
      if (this.storage.channels[i] === undefined) {
        this.storage.channels[i] = {
          offset: 0,
          weight: 1
        }
      }
    }
  }

  _diValue = 0;

  async actionUpdateChannel (data) {
    this.storage.channels[data.channel].offset = data.offset
    this.storage.channels[data.channel].weight = data.weight
    this.save()
  }

  async update () {
    for (var ch = 1; ch <= 20; ch++) {
      if (!this.params.eachGate && !this.outputs['cn' + ch].connected) continue
      try {
        var resp = await this.client.commandPromise(1, 0x03, 0xA0 + ((+ch - 1) * 2), 0x02)
      } catch (err) {
        this.error('Error request', err)
        break
      }

      var one = resp.data.slice(0, 2)
      var two = resp.data.slice(2, 4)
      resp.data = Buffer.concat([two, one], 4)
      var counterResult = resp.data.readUInt32BE()
      const conf = this.storage.channels[ch]
      counterResult = (counterResult * conf.weight) + conf.offset
      this.shares.cn[ch - 1] = counterResult
      this.outputs['cn' + ch].push(counterResult)
    }
    this.shares.progress = false
    this.render()
  }
}
