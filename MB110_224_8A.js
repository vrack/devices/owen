const modbusRTU = require('./modbus/modbus')
const MB110Baisc = require('./MB110Basic')
const { Rule, Port } = require('vrack-core')

module.exports = class MB110_101 extends MB110Baisc {
  ports () {
    return [
      new Port('provider').input().data('Provider')
        .description('Вход для провайдера'),
      new Port('provider').output().data('Provider')
        .description('Выход для провайдера'),
      new Port('adc%d').output().data('number').dynamic(8)
        .description('Значение дискретного входа')
    ]
  }

  checkParams () {
    return [
      new Rule('address').required().default(1).isInteger().expression('value >= 0'),
      new Rule('eachGate').required().default(true).isBoolean(),
      new Rule('timeout').required().default(5000).isInteger().expression('value >= 0'),
      new Rule('inputs').required().default(0xFF).isInteger().expression('value >= 0')
    ]
  }

  settings () {
    return {
      shares: true,
      messageTypes: ['error', 'event']
    }
  }

    shares = {
      online: false,
      progress: false,
      adc: [0, 0, 0, 0, 0, 0, 0, 0],
      adcErrors: [0, 0, 0, 0, 0, 0, 0, 0]
    }

    CHANNEL_ERRORS = {
      0xF000: 'Значение заведомо неверно',
      0xF006: 'Данные не готовы. Необходимо дождаться результатов первого измерения после включения прибора',
      0xF007: 'Датчик отключен',
      0xF008: 'Велика температура свободных концов ТП',
      0xF009: 'Мала температура свободных концов ТП',
      0xF00A: 'Измеренное значение слишком велико',
      0xF00B: 'Измеренное значение слишком мало',
      0xF00C: 'Короткое замыкание датчика',
      0xF00D: 'Обрыв датчика',
      0xF00E: 'Отсутствие связи с АЦП',
      0xF00F: 'Некорректный калибровочный коэффициент'
    };

    async update () {
      for (let i = 0; i < 8; i++) {
        if (!this._provider.state.connected) break
        const channel = i + 1
        if (this.params.inputs !== 0 && !((this.params.inputs >> i) & 1)) continue
        if (this.params.inputs === 0 && !this.outputs['adc' + channel].connected) continue
        await this.updateRequest(i, channel)
      }
    }

    async updateRequest (i, channel) {
      const reqBuffer = modbusRTU.makeRequest(this.params.address, 4, this._offsetChannelMap(channel) + 2, 4)
      this.shares.online = true
      await this._provider.autoRequest(reqBuffer, this.params.timeout, 3)
      const resp = modbusRTU.splitPackage(this._provider.getBuffer())
      var error = resp.data.readUInt16BE(0)
      if (!this.CHANNEL_ERRORS[error]) {
        if (this.shares.adcErrors[i] !== 0) this.shares.adcErrors[i] = 0
        const value = resp.data.readFloatBE(4).toFixed(4)
        if (this.params.eachGate || this.shares.adc[i] !== value) {
          this.shares.adc[i] = value
          this.outputs['adc' + channel].push(this.shares.adc[i])
        }
      } else {
        this.shares.adcErrors[i] = error
        this.error('ADC channel ' + channel + ' error', new Error(this.CHANNEL_ERRORS[error]))
      }
    }

    _offsetChannelMap (channel) {
      return (channel - 1) * 6
    }
}
