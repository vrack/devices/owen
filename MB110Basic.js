
const crc = require('./modbus/crc16')
const VRack = require('vrack-core')
module.exports = class extends VRack.Device {
    _provider = {}
    async inputProvider (data) {
      this._provider = data
      if (this.shares.progress) return ''

      this._provider.setPkgCheck((buffer) => {
        if (buffer.length > 5) {
          if (buffer.readUInt8(1) === 0x10) { if (buffer.length === 8) return true; else return false }
          if (buffer.readUInt8(1) === 0x0f) { if (buffer.length === 8) return true; else return false }
          if (buffer.readUInt8(2) === (buffer.length - 5)) if (crc.check(buffer)) return true
          return false
        }
      })

      this.shares.progress = true
      this.render()
      try {
        await this.update()
        this.shares.online = true
      } catch (err) {
        this.shares.online = false
        this.error('Update device error', err)
      }

      this.shares.progress = false
      this.outGate()
    }

    async update () { }

    outGate () {
      this.outputs.provider.push(this._provider)
      this.render()
    }
}
