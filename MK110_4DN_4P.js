const modbusRTU = require('./modbus/modbus')
const MB110Baisc = require('./MB110Basic')
const { Rule, Port, Action } = require('vrack-core')

module.exports = class MB110_101 extends MB110Baisc {
  ports () {
    return [
      new Port('gate').input().data('signal')
        .description('Вход для провайдера'),
      new Port('do%d').input().data('number').dynamic(4)
        .description('Значение дискретного входа'),
      new Port('di%d').output().data('number').dynamic(4)
        .description('Значение дискретного входа'),
      new Port('provider').output().data('Provider')
        .description('Значение дискретного входа')
    ]
  }

  checkParams () {
    return [
      new Rule('address').required().default(1).isInteger().expression('value >= 0'),
      new Rule('eachGate').required().default(false).isBoolean(),
      new Rule('timeout').required().default(5000).isInteger().expression('value >= 0')
    ]
  }

  settings () {
    return {
      storage: false,
      messageTypes: ['error', 'event']
    }
  }

  preProcess () {
    for (let i = 1; i <= 4; i++) {
      this['inputDo' + i] = (data) => {
        if (data) {
          this._doWrite = this._doWrite | (1 << (i - 1))
        } else {
          this._doWrite = this._doWrite & (~(1 << (i - 1)))
        }
      }
    }
    this.outputIDS.push('provider')
  }

  shares = {
    online: false,
    progress: false,
    di: [0, 0, 0, 0],
    do: [0, 0, 0, 0],
    inputMask: 0,
    outputMask: 0
  }

  _diValue = 0;
  _doValue = 0;
  _doWrite = 0;
  async update () {
    await this.updateInput()
    await this.updateOutput()
    await this.writeOutput()
  }

  async updateInput () {
    const resp = await this.command(0x33)
    var bitmask = resp.data.readUInt16BE()
    this.shares.inputMask = bitmask
    if (this._diValue !== bitmask || this.params.eachGate) {
      for (let i = 0; i < 4; i++) {
        const s = (bitmask >> i) & 1
        if (this.shares.di[i] !== s || this.params.eachGate) {
          this.shares.di[i] = s
          this.outputs['di' + (i + 1)].push(s)
        }
      }
      this._diacValue = bitmask
    }
  }

  async updateOutput () {
    const resp = await this.command(0x32)
    var bitmask = resp.data.readUInt16BE()
    this.shares.outputMask = bitmask
    if (this._doValue !== bitmask) {
      for (let i = 0; i < 4; i++) {
        const s = (bitmask >> i) & 1
        if (this.shares.do[i] !== s) this.shares.do[i] = s
      }
      this._doValue = bitmask
    }
  }

  async writeOutput () {
    if (this._doWrite !== this._doValue) {
      const bf = Buffer.alloc(2)
      bf.writeUInt16BE(this._doWrite)
      const reqBuffer = modbusRTU.makeRequest(this.params.address, 0x10, 0x32, 0x01, 'c', bf.toString('binary'))
      await this._provider.autoRequest(reqBuffer, this.params.timeout, 1)
    }
  }

  async command (cmd) {
    const reqBuffer = modbusRTU.makeRequest(this.params.address, 3, cmd, 1)
    await this._provider.autoRequest(reqBuffer, this.params.timeout, 1)
    return modbusRTU.splitPackage(this._provider.getBuffer())
  }
}
