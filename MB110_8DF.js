const modbusRTU = require('./modbus/modbus')
const MB110Baisc = require('./MB110Basic')
const { Rule, Port } = require('vrack-core')

module.exports = class MB110_101 extends MB110Baisc {
  ports () {
    return [
      new Port('provider').input().data('Provider')
        .description('Вход для провайдера'),
      new Port('provider').output().data('Provider')
        .description('Выход для провайдера'),
      new Port('diac%d').output().data('number').dynamic(8)
        .description('Значение дискретного входа')
    ]
  }

  checkParams () {
    return [
      new Rule('address').required().default(1).isInteger().expression('value >= 0'),
      new Rule('eachGate').required().default(true).isBoolean(),
      new Rule('timeout').required().default(5000).isInteger().expression('value >= 0')
    ]
  }

  settings () {
    return {
      shares: true,
      messageTypes: ['error', 'event']
    }
  }

  shares = {
    online: false,
    progress: false,
    diac: [0, 0, 0, 0, 0, 0, 0, 0],
    bitmask: 0
  }

  _diacValue = 0;

  async update () {
    const reqBuffer = modbusRTU.makeRequest(this.params.address, 3, 0x33, 1)
    await this._provider.autoRequest(reqBuffer, this.params.timeout, 3)
    const resp = modbusRTU.splitPackage(this._provider.getBuffer())
    var bitmask = resp.data.readUInt16BE()
    this.shares.bitmask = bitmask
    if (this._diacValue !== bitmask || this.params.eachGate) {
      for (let i = 0; i < 8; i++) {
        const s = (bitmask >> i) & 1
        if (this.shares.diac[i] !== s || this.params.eachGate) {
          this.shares.diac[i] = s
          this.outputs['diac' + (i + 1)].push(s)
        }
      }
      this._diacValue = bitmask
    }
  }
}
