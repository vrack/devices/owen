
const VMB110Basic = require('./VMB110Basic')
const { Rule, Port } = require('vrack-core')
module.exports = class MB110_101 extends VMB110Basic {
  ports () {
    return [
      new Port('buffer').input().data('Buffer')
        .description('Данные от сервера'),
      new Port('buffer').output().data('Buffer')
        .description('Данные для отправки')
    ]
  }

  checkParams () {
    return [
      new Rule('address').default(1).isInteger().expression('value >= 0'),
      new Rule('timeout').default(3000).isInteger().expression('value >= 0')
    ]
  }

  settings () {
    return {
      message: false,
      shares: true
    }
  }

  shares = {
    diac: [0, 0, 0, 0, 0, 0, 0, 0],
    bitmask: 0
  }

  updateChannels () {
    const bitmask = Math.floor(0 + Math.random() * (255 + 1))
    this.shares.bitmask = bitmask
    for (const bit in this.shares.diac) {
      this.shares.diac[bit] = (bitmask >> bit) & 1
    }
    this._coils.writeUInt16BE(bitmask, 0x66)
  }
}
