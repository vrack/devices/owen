
const VMB110Basic = require('./MB110Basic')
const crc = require('./modbus/crc16')

module.exports = class MB110_101 extends VMB110Basic {
    _buffer = Buffer.from('');
    _coils = Buffer.alloc(256);
    _timer = false;

    process () {
      setInterval(() => {
        this.updateChannels()
        this.render()
      }, this.params.timeout)
    }

    updateBufferTimer () {
      if (this._timer) clearTimeout(this._timer)
      this._timer = setTimeout(() => {
        this._buffer = Buffer.from('')
        this._timer = false
      }, 500)
    }

    inputBuffer (data) {
      this.updateBufferTimer()
      this._buffer = Buffer.concat([this._buffer, data])
      if (this._buffer.length === 8) {
        if (this._buffer.readUInt8(0) === this.params.address) {
          const start = this._buffer.readInt16BE(2)
          const count = this._buffer.readInt16BE(4)
          const countBytes = count * 2
          let sendBuf = Buffer.alloc(3)
          sendBuf.writeUInt8(this.params.address, 0)
          sendBuf.writeUInt8(this._buffer.readUInt8(1), 1)
          sendBuf.writeUInt8(countBytes, 2)
          sendBuf = Buffer.concat([sendBuf, this._coils.slice(start * 2, start * 2 + countBytes), Buffer.alloc(2)])
          crc.add(sendBuf)
          clearTimeout(this._timer)
          this._buffer = Buffer.from('')
          this.outputs.buffer.push(sendBuf)
        } else {
          clearTimeout(this._timer)
          this._buffer = Buffer.from('')
        }
      }
    }
}
