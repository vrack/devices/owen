const MB210Basic = require('./MB210Basic')
const { Rule, Port } = require('vrack-core')
module.exports = class MB210_221 extends MB210Basic {
  ports () {
    return [
      new Port('gate').input().data('signal')
        .description('Вход для провайдера'),
      new Port('do%d').input().data('number').dynamic(8)
        .description('Значение дискретного входа'),
      new Port('di%d').output().data('number').dynamic(6)
        .description('Значение дискретного входа')
    ]
  }

  checkParams () {
    return [
      new Rule('host').required().default('192.168.0.7').isString(),
      new Rule('port').required().default(502).isInteger().expression('value >= 0'),
      new Rule('eachGate').required().default(false).isBoolean(),
      new Rule('invert').required().default(false).isBoolean(),
      new Rule('DOChannels').required().default(0xFF)
    ]
  }

  settings () {
    return {
      storage: false,
      messageTypes: ['error', 'event']
    }
  }

  shares = {
    online: false,
    progress: false,
    di: [0, 0, 0, 0, 0, 0],
    do: [0, 0, 0, 0, 0, 0, 0, 0]
  }

  preProcess () {
    for (let i = 1; i <= 8; i++) {
      this['inputDo' + i] = (data) => {
        if (data & 1) this._doWrite = this._doWrite | (1 << (i - 1))
        else this._doWrite = this._doWrite & (~(1 << (i - 1)))
      }
    }
  }

  _diValue = 0;
  _doValue = 0;
  _doWrite = 0;
  async update () {
    try {
      await this.updateDI()
      await this.updateDO()
      await this.writeDO()
    } catch (error) {
      this.error('Error update', error)
    }
    this.render()
    this.shares.progress = false
  }

  async updateDI () {
    var resp = await this.client.commandPromise(1, 3, 0x33, 1)
    var bitmask = resp.data.readUInt16BE()
    if (this.params.invert) bitmask = ~bitmask
    if (this._diValue !== bitmask || this.params.eachGate) {
      for (let i = 0; i < 6; i++) {
        const s = (bitmask >> i) & 1
        if (this.shares.di[i] !== s || this.params.eachGate) {
          this.shares.di[i] = s
          this.outputs['di' + (i + 1)].push(s)
        }
      }
      this._diValue = bitmask
    }
  }

  async updateDO () {
    var resp = await this.client.commandPromise(1, 3, 0x1D4, 1)
    var bitmask = resp.data.readUInt16BE()
    if (this._doValue !== bitmask) {
      for (let i = 0; i < 8; i++) {
        const s = (bitmask >> i) & 1
        if (this.shares.do[i] !== s) this.shares.do[i] = s
      }
      this._doValue = bitmask
    }
  }

  async writeDO () {
    const doWrite = this._doWrite & this.params.DOChannels
    const doDiff = this._doValue & this.params.DOChannels
    if (doDiff === doWrite) return
    const myMask = (~this.params.DOChannels & 0xFF)
    const doValue = myMask & this._doValue
    await this.client.commandPromise(1, 0x06, 0x1D6, doValue | doWrite)
  }
}
