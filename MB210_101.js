
const MB210Basic = require('./MB210Basic')
const { Rule, Port } = require('vrack-core')
module.exports = class MB210_101 extends MB210Basic {
  ports () {
    return [
      new Port('gate').input().data('signal')
        .description('Вход для провайдера'),
      new Port('adc%d').output().data('number').dynamic(8)
        .description('Значение дискретного входа')
    ]
  }

  checkParams () {
    return [
      new Rule('host').required().default('192.168.0.7').isString(),
      new Rule('port').required().default(502).isInteger().expression('value >= 0'),
      new Rule('eachGate').required().default(true).isBoolean(),
      new Rule('inputs').required().default(0xFF).isInteger().expression('value >= 0')
    ]
  }

  settings () {
    return {
      storage: false,
      messageTypes: ['error', 'event']
    }
  }

  shares = {
    online: false,
    progress: false,
    adc: [0, 0, 0, 0, 0, 0, 0, 0],
    adcErrors: [0, 0, 0, 0, 0, 0, 0, 0]
  }

  CHANNEL_ERRORS = {
    0xF0: 'Значение заведомо неверно',
    0xF6: 'Данные не готовы. Необходимо дождаться результатов первого измерения после включения прибора',
    0xF7: 'Датчик отключен',
    0xF8: 'Велика температура свободных концов ТП',
    0xF9: 'Мала температура свободных концов ТП',
    0xFA: 'Измеренное значение слишком велико',
    0xFB: 'Измеренное значение слишком мало',
    0xFC: 'Короткое замыкание датчика',
    0xFD: 'Обрыв датчика',
    0xFE: 'Отсутствие связи с АЦП',
    0xFF: 'Некорректный калибровочный коэффициент'
  };

  update () {
    this.client.commandPromise(1, 3, 4000, 24)
      .then((resp) => {
        var buffer = resp.data
        for (let i = 0; i < 8; i++) {
          const channel = i + 1
          if (this.params.inputs !== 0 && !((this.params.inputs >> i) & 1)) continue
          if (this.params.inputs === 0 && !this.outputs['adc' + channel].connected) continue
          var subuf = buffer.slice(i * 6, (i * 6) + 4)
          var one = subuf.slice(0, 2)
          var two = subuf.slice(2, 4)
          subuf = Buffer.concat([two, one], 4)
          var error = subuf.readUInt8(0)
          if (!this.CHANNEL_ERRORS[error]) {
            if (this.shares.adcErrors[i] !== 0) this.shares.adcErrors[i] = 0
            const value = (subuf.readFloatBE(0)).toFixed(4)
            if (this.params.eachGate || this.shares.adc[i] !== value) {
              this.shares.adc[i] = value
              this.outputs['adc' + channel].push(this.shares.adc[i])
            }
          } else {
            this.shares.adcErrors[i] = error
            this.error('ADC channel ' + channel + ' error', new Error(this.CHANNEL_ERRORS[error] + ' CODE [' + error + ']'))
          }
        }
        this.shares.progress = false
        this.render()
      }).catch((error) => {
        this.error('Error request', error)
        this.shares.progress = false
        this.render()
      })
  }
}
