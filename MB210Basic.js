// const modbus = require('jsmodbus')
const net = require('net')
const ModbusTCP = require('./modbus/modbusTCP')
const VRack = require('vrack-core')
module.exports = class extends VRack.Device {
  outputIDS = [];
  inputIDS = ['gate'];

  shares = {
    online: false
  }

  socket = false;
  client = false;

  process () {
    this.socket = new net.Socket()
    this.client = new ModbusTCP(this.socket)
    this.socket.on('connect', () => {
      this.event('connect')
      this.shares.online = true
      this.update()
      this.render()
    })
    this.socket.on('close', (data) => {
      this.event('disconnect')
      this.shares.online = false
      this.render()
      setTimeout(this.socketConnect.bind(this), 1000)
    })
    this.socket.on('error', data => this.error('Socket Error', data))
    this.socketConnect()
    this.render()
  }

  socketConnect () {
    this.socket.connect({
      host: this.params.host,
      port: this.params.port
    })
  }

  update () {}

  // ***********************   INPUTS   ********************** //
  inputGate () {
    if (this.shares.progress || !this.shares.online) return
    this.shares.progress = true
    this.update()
  }
}
