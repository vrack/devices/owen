var crc = require('./crc16')

module.exports = {
  makeRequest (slave, func, addr, b45, type, data) {
    var bytes
    switch (type) {
      case 'sb':
        bytes = 1
        break
      case 'b':
        bytes = b45 >> 3
        if (b45 & 0x07) { ++bytes }
        break
      case 'w':
        bytes = b45 * 2
        break
      case 'c':
        bytes = b45 * 2
        break
    }
    var buff = Buffer.alloc(bytes ? 9 + bytes : 8)
    buff[0] = slave
    buff[1] = func
    buff.writeUInt16BE(addr, 2)
    buff.writeUInt16BE(b45, 4)
    if (bytes) { buff[6] = bytes }
    var i
    switch (type) {
      case 'sb':
        buff.writeUInt8(data, 7)
        break
      case 'b':
        for (i = 0; i < b45; ++i) {
          var idx = 7 + (i >> 3)
          if (!(i & 0x07)) { buff[idx] = 0 }
          if (data[i]) { buff[idx] |= 1 << (i & 0x07) }
        }
        break
      case 'w':
        for (i = 0; i < b45; ++i) { buff.writeUInt16BE(data[i], 7 + i * 2) }
        break
      case 'c':
        buff.write(data, 7, 'binary')
        break
    }
    crc.add(buff)
    return buff
  },

  splitPackage (buff) {
    const result = {}
    result.address = buff.readInt8(0)
    result.command = buff.readInt8(0)
    result.size = buff.readInt8(0)
    result.data = buff.slice(3, buff.length - 2)
    result.crc = buff.slice(buff.length - 2)
    return result
  }
}
