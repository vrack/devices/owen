const MB210Basic = require('./MB210Basic')
const { Rule, Port } = require('vrack-core')
module.exports = class MB210_221 extends MB210Basic {
  ports () {
    return [
      new Port('gate').input().data('Provider')
        .description('Вход для провайдера'),
      new Port('di%d').output().data('boolean').dynamic(6)
        .description('Вход для провайдера'),
      new Port('diac%d').output().data('number').dynamic(9)
        .description('Значение дискретного входа')
    ]
  }

  checkParams () {
    return [
      new Rule('eachGate').required().default(false).isBoolean(),
      new Rule('invert').required().default(false).isBoolean()
    ]
  }

  settings () {
    return {
      storage: false,
      messageTypes: ['error', 'event']
    }
  }

  shares = {
    online: false,
    progress: false,
    di: [0, 0, 0, 0, 0, 0],
    diac: [0, 0, 0, 0, 0, 0, 0, 0, 0]
  }

  _diValue = 0;
  _diacValue = 0;

  update () {
    this.client.commandPromise(1, 3, 0x33, 1)
      .then((resp) => {
        var bitmask = resp.data.readUInt16BE()
        if (this.params.invert) bitmask = ~bitmask
        if (this._diValue !== bitmask || this.params.eachGate) {
          for (let i = 0; i < 6; i++) {
            const s = (bitmask >> i) & 1
            if (this.shares.di[i] !== s || this.params.eachGate) {
              this.shares.di[i] = s
              this.outputs['di' + (i + 1)].push(s)
            }
          }
          this.render()
          this._diValue = bitmask
        }
        return this.client.commandPromise(1, 3, 0x1388, 1)
      }).then((resp) => {
        var bitmask = resp.data.readUInt16BE()
        if (this.params.invert) bitmask = ~bitmask
        if (this._diacValue !== bitmask || this.params.eachGate) {
          for (let i = 0; i < 9; i++) {
            const s = (bitmask >> i) & 1
            if (this.shares.diac[i] !== s || this.params.eachGate) {
              this.shares.diac[i] = s
              this.outputs['diac' + (i + 1)].push(s)
            }
          }
          this.render()
          this._diacValue = bitmask
        }
        this.shares.progress = false
      }).catch((error) => {
        this.error('Error request', error)
        this.shares.progress = false
      })
  }
}
