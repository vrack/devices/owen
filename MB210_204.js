const MB210Basic = require('./MB210Basic')
const { Rule, Port } = require('vrack-core')
module.exports = class MB210_221 extends MB210Basic {
  ports () {
    return [
      new Port('gate').input().data('signal')
        .description('Вход для провайдера'),
      new Port('di%d').output().data('number').dynamic(20)
        .description('Значение дискретного входа')
    ]
  }

  checkParams () {
    return [
      new Rule('host').required().default('192.168.0.7').isString(),
      new Rule('port').required().default(502).isInteger().expression('value >= 0'),
      new Rule('eachGate').required().default(false).isBoolean(),
      new Rule('invert').required().default(false).isBoolean()
    ]
  }

  settings () {
    return {
      messageTypes: ['error', 'event'],
      shares: true
    }
  }

  shares = {
    online: false,
    progress: false,
    di: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  }

  preProcess () {
    for (let i = 1; i <= 20; i++) this.outputIDS.push('di' + i)
  }

  _diValue = 0;

  update () {
    this.client.commandPromise(1, 3, 0x33, 2)
      .then((resp) => {
        var one = resp.data.slice(0, 2)
        var two = resp.data.slice(2, 4)
        resp.data = Buffer.concat([two, one], 4)
        var bitmask = resp.data.readUInt32BE()
        if (this.params.invert) bitmask = ~bitmask
        if (this._diValue !== bitmask || this.params.eachGate) {
          for (let i = 0; i < 20; i++) {
            const s = (bitmask >> i) & 1
            if (this.shares.di[i] !== s || this.params.eachGate) {
              this.shares.di[i] = s
              this.outputs['di' + (i + 1)].push(s)
            }
          }
          this.render()
          this._diValue = bitmask
        }
        this.shares.progress = false
      }).catch((error) => {
        this.error('Error request', error)
        this.shares.progress = false
      })
  }
}
