
const { Rule } = require('vrack-core')
const VMB110Basic = require('./VMB110Basic')
module.exports = class MB110_101 extends VMB110Basic {
  checkParams () {
    return [
      new Rule('address').default(1).isInteger().expression('value >= 0'),
      new Rule('timeout').default(5000).isInteger().expression('value >= 0'),
      new Rule('min').default(-100).isInteger(),
      new Rule('max').default(100).isInteger(),
      new Rule('dvd').default(10).isInteger()
    ]
  }

  settings () {
    return {
      storage: false,
      message: false
    }
  }

  shares = {
    adc: [0, 0, 0, 0, 0, 0, 0, 0]
  }

  updateChannels () {
    for (const key in this.shares.adc) {
      this.shares.adc[key] = Math.floor(this.params.min + Math.random() * (this.params.max - this.params.min + 1)) / this.params.dvd
      this._coils.writeFloatBE(this.shares.adc[key], key * 12 + 8)
    }
  }
}
