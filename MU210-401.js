const MB210Basic = require('./MB210Basic')
const { Rule, Port } = require('vrack-core')
module.exports = class MB210_221 extends MB210Basic {
  checkParams () {
    return [

      new Rule('host').required().default('192.168.0.7').isString(),
      new Rule('port').required().default(502).isInteger().expression('value >= 0')
    ]
  }

  settings () {
    return {
      shares: true,
      messageTypes: ['error', 'event']
    }
  }

  shares = {
    online: false,
    progress: false,
    do: [0, 0, 0, 0, 0, 0, 0, 0]
  }

  ports () {
    return [
      new Port('gate').input().data('signal')
        .description('Вход для провайдера'),
      new Port('do%d').input().data('number').dynamic(8)
        .description('Значение дискретного выхода')
    ]
  }

  preProcess () {
    for (let i = 1; i <= 8; i++) {
      this.inputIDS.push('do' + i)
      this['inputDo' + i] = (data) => {
        if (data & 1) this._doWrite = this._doWrite | (1 << (i - 1))
        else this._doWrite = this._doWrite & (~(1 << (i - 1)))
      }
    }
  }

  _doValue = 0;
  _doWrite = 0;

  async update () {
    try {
      await this.updateDO()
      await this.writeDO()
    } catch (error) {
      this.error('Error update', error)
    }
    this.render()
    this.shares.progress = false
  }

  async updateDO () {
    var resp = await this.client.commandPromise(1, 3, 0x1D4, 1)
    var bitmask = resp.data.readUInt16BE()
    if (this._doValue !== bitmask) {
      for (let i = 0; i < 8; i++) {
        const s = (bitmask >> i) & 1
        if (this.shares.do[i] !== s) this.shares.do[i] = s
      }
      this._doValue = bitmask
    }
  }

  async writeDO () {
    if (this._doWrite === this._doValue) return
    await this.client.commandPromise(1, 0x06, 0x1D6, this._doWrite)
  }
}
